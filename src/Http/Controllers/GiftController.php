<?php

namespace Coredevina\Giftquota\Http\Controllers;

use App\Http\Controllers\Controller;
use Coredevina\Giftquota\Giftquota as Giftquota;
use Illuminate\Http\Request;
use Coredevina\Giftquota\Models\giftQuotaTransaction;

use Illuminate\Support\Facades\Log;

class GiftController extends Controller
{
    // Get All Packages 
    public function Get_All_Packages(Request $request){
        $parameters=array(
            "url"=>"https://api.mycontents.id/user/getallpackages",
            "method"=>"GET"
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    }     
    // Get Current Balance 
    public function Get_Current_Balance(Request $request){
        $parameters=array(
            "url"=>"https://api.mycontents.id/user/getbalance",
            "method"=>"POST"
        );
        
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    } 
    
    // Send Data Gift 
    public function Send_Data_Gift(Request $request){
        Log::info('http_request_giftQuota :'.json_encode($request->all()));
        $parameters=array(
            "url"=>"https://api.mycontents.id/user/sendgiftdatabywallet",
            "method"=>"POST",
            "data_body"=>array("packageid"=>$request->packageid,"toneid"=>$request->toneid,"sdc"=>$request->sdc,"msisdn"=>$request->msisdn) 
        );
        $data=new Giftquota();
        $data->Send_Data_Gift($parameters);
        // $data='{"t_gift_msisdn":"6285773085819","t_gift_toneid":0,"t_gift_sdc":"99878","m_package_id":3,"t_gift_code":"RYL16018789460286","t_data_id":0,"m_account_id":174,"t_gift_status":0,"t_gift_message":"success"}';
        Log::info('http_response_giftQuota : '.$data);
        $response=json_decode($data,true);
        if (isset($request) && $response['t_gift_message']=='success') {
            $transaction= new giftQuotaTransaction(); 
            $transaction->msisdn=$request['msisdn'];
            $transaction->quota=$request['quota'];
            $transaction->app_id=$request['app_id'];
            $transaction->package_id=$request['package_id'];
            $transaction->package_name=$request['package_name'];
            $transaction->package_code=$request['package_code'];
            $transaction->package_description=$request['package_description'];
            $transaction->package_amount=$request['package_amount'];
            $transaction->status=$response['t_gift_message'];
            $transaction->t_gift_code=$response['t_gift_code'];
            $transaction->save();
            Log::info('db Transaction : '.json_encode($transaction));
        }
        return $response;
    } 
    // Get 10 Latest Gift History
    public function Get_10_Latest_Gift_History(Request $request){
        $parameters=array(
            "url"=>"https://api.mycontents.id/user/getlatestgifthistory",
            "method"=>"GET"
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    } 
    // Get 10 Latest Wallet History 
    public function Get_10_Latest_Wallet_History(Request $request){
        $parameters=array(
            "url"=>"https://api.mycontents.id/user/getlatestwallethistory",
            "method"=>"GET"
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    }
    // Get Gift History By Date 
    public function Get_Gift_History_By_Date(Request $request){
        $parameters=array(
            "url"=>"https://api.mycontents.id/user/getwallethistorybydate",
            "method"=>"GET",
            "datefrom"=>$request->datefrom,
            "dateto"=>$request->dateto
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    }
    // Wallet History By Date 
    public function Get_Wallet_History_By_Date(Request $request){
        $parameters=array(
            "url"=>"https://api.mycontents.id/user/getwallethistorybydate",
            "method"=>"GET",
            "datefrom"=>$request->datefrom,
            "dateto"=>$request->dateto
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    }
    // Get All Price 
    public function Get_All_Price(Request $request){
        $parameters=array(
            "url"=>" https://api.mycontents.id/user/getprice",
            "method"=>"GET"
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    }
    // Get All Rank
    public function Get_All_Rank(Request $request){
        $parameters=array(
            "url"=>" https://api.mycontents.id/user/getrank",
            "method"=>"GET"
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    } 
    // Request Wallet Deposit 
    public function Request_Wallet_Deposit(Request $request){
        $parameters=array(
            "url"=>" https://api.mycontents.id/user//requestwalletdeposit",
            "method"=>"POST",
            "amount"=>$request->amount
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    }
    // Get Wallet Deposit 
    public function Get_Wallet_Deposit(Request $request){
        $parameters=array(
            "url"=>" https://api.mycontents.id/user/getwalletdeposit",
            "method"=>"POST",
            "amount"=>$request->amount
        );
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;
    }

    public function drListener(Request $request){
        Log::info('DR Listener Mycontent Callback :'.json_encode($request->all()));
    }

}
