<?php

// namespace App\Models\Models;
namespace Coredevina\Giftquota\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class giftQuotaTransaction extends Model
{
    use HasFactory;
    protected $table='gift_quota_transactions';
    protected $fillable=array('msisdn','quota','app_id','package_id','package_name','package_code','package_description','package_amount','status','t_gift_code');
    protected $casts = array('msisdn'=>'integer','quota'=>'integer','app_id'=>'integer','package_id'=>'integer','package_amount');


}
