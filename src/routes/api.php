<?php
// Route::group(['namespace' => 'Coredevina\Gifequota\Http\Controllers', 'prefix' => 'api'], function () {
//     Route::post('generateAuth ', 'GifeController@generateAccesAuthorization')->name('generateAuth');
// });


$this->app->router->group(['namespace'=>'Coredevina\Giftquota\Http\Controllers','prefix'=>'api','middleware' => 'app'],function(){
    $this->app->router->post('v1/gqAll_Packages ', 'GiftController@Get_All_Packages');
    $this->app->router->post('v1/gqCurrentBalance ', 'GiftController@Get_Current_Balance');
    $this->app->router->post('v1/gqSendData ', 'GiftController@Send_Data_Gift');
    $this->app->router->post('v1/gqLatest10Gift ', 'GiftController@Get_10_Latest_Gift_History');
    $this->app->router->post('v1/gqLatest10Wallet ', 'GiftController@Get_10_Latest_Wallet_History');
    $this->app->router->post('v1/gqGiftHistoryByDate ', 'GiftController@Get_Gift_History_By_Date');
    $this->app->router->post('v1/gqWalletHistoryByDate ', 'GiftController@Get_Wallet_History_By_Date');
    $this->app->router->post('v1/gqAllPrice ', 'GiftController@Get_All_Price');
    $this->app->router->post('v1/gqAllRank ', 'GiftController@Get_All_Rank');
    $this->app->router->post('v1/gqRequestWalleDeposit ', 'GiftController@Request_Wallet_Deposit');
    $this->app->router->post('v1/gqGetWalletDeposit ', 'GiftController@Get_Wallet_Deposit');
    // DR
    $this->app->router->post('v1/giftQuota_callback ', 'GiftController@drListener');

    
});
