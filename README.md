## GiftQuota

```
composer require coredevina/giftquota
```


##Lumen

add source below to  bootstrap/app.php
```
$app->register(Coredevina\GiftQuota\GiftquotaServiceProvider::class);


if (!class_exists('GiftQuota')) {
    class_alias('\Coredevina\GiftQuota\Giftquota', 'GiftQuota');
}

....

$app->configure('giftQuota');

```

```
php artisan vendor:publish --provider="Coredevina\Giftquota\GiftquotaServiceProvider"
```


Change SDP baseUrl on config/sdp.php
```
return[
    'api' => 'url vendor'
];
```

SomeController.php
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GiftQuota;

...
```

Use
```

$parameters=array(
            "url"=>"https://api.mycontents.id/user/getallpackages",
            "method"=>"GET"
);
        $data=new Giftquota();
        $data->curl_http_request($parameters);
        return $data;

```
