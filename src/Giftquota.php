<?php
namespace Coredevina\Giftquota;
// use App\Models\Models\giftQuotaTransaction;


class Giftquota 
{

       // Get All Packages 
       public function Get_All_Packages($data){
        $response=$this->curl_http_request($data);
        return $response;
    }     
    // Get Current Balance 
    public function Get_Current_Balance($data){
        $response=$this->curl_http_request($data);
        return $response;
    } 
    
    // Send Data Gift 
    public function Send_Data_Gift($data){
        $response=$this->curl_http_request($data);
        return $response;
    } 
    // Get 10 Latest Gift History
    public function Get_10_Latest_Gift_History($data){
        $response=$this->curl_http_request($data);
        return $response;
    } 
    // Get 10 Latest Wallet History 
    public function Get_10_Latest_Wallet_History($data){
        $response=$this->curl_http_request($data);
        return $response;
    }
    // Get Gift History By Date 
    public function Get_Gift_History_By_Date($data){
        $response=$this->curl_http_request($data);
        return $response;
    }
    // Wallet History By Date 
    public function Get_Wallet_History_By_Date($data){

        $response=$this->curl_http_request($data);
        return $response;
    }
    // Get All Price 
    public function Get_All_Price($data){
        $response=$this->curl_http_request($data);
        return $response;
    }
    // Get All Rank
    public function Get_All_Rank($data){
        
        $response=$this->curl_http_request($data);
        return $response;
    } 
    // Request Wallet Deposit 
    public function Request_Wallet_Deposit($data){
        $response=$this->curl_http_request($data);
        return $response;
    }
    // Get Wallet Deposit 
    public function Get_Wallet_Deposit($data){
        $response=$this->curl_http_request($data);
        return $response;
    }
    // curl http_request
    function curl_http_request($data){
        if (isset($data['data_body'])) {
            if ($data['data_body']['toneid']==null) {
                $data['data_body']['toneid']="";
            }
            $databody=$data['data_body'];
            
        }else{
            $databody=array();
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $data['url'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_POSTFIELDS =>$databody,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $data['method'],
            CURLOPT_HTTPHEADER => array(
              "Authorization:".base64_encode('1e4e5e765916f952ad3eeb3396a69f3d:8022715694151dbf48c9cad4f575df70f4e34c11f0d6861cc7bc52edf0d406f9'),
            ),
          ));
          $response = curl_exec($curl);
          
          curl_close($curl);
          echo $response;
          
    }
}
