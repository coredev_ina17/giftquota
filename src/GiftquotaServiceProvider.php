<?php

namespace Coredevina\Giftquota;

use Illuminate\Support\ServiceProvider;

class GiftquotaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->mergeConfigFrom(
            __DIR__.'/config/giftquota.php','giftquota'
        );
        // $this->publishes([
        //     __DIR__.'/config/giftquota.php' => app()->basePath('config/giftquota.php'),
        // ]);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //  $this->loadMigrationsFrom(__DIR__.'/path/to/migrations');
        if ($this->app->runningInConsole()) {
            // publish config file
            // register artisan command
            if (! class_exists('CreateGiftQuotaTransactionsTable')) {
              $this->publishes([
                __DIR__ . '/database/migrations/create_gift_quota_transactions_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_gift_quota_transactions_table.php'),
                // you can add any number of migrations here
              ], 'migrations');
            }
          }
    }
}
